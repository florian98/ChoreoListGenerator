package de.flboehm.choreoListGenerator.controller;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import de.flboehm.choreoListGenerator.model.Choreography;
import de.flboehm.choreoListGenerator.model.Position;
import de.flboehm.choreoListGenerator.model.Scene;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Class for generator
 */
public class Generator {

  /**
   * Run generator. Very simple command line tool.
   *
   * @param args arguments
   */
  public static void main(String[] args) {
    String dancerId;
    System.out.println("Input file:");
    Choreography choreography;
    try (Scanner scn = new Scanner(System.in)) {
      choreography = parseChoreographyFromFile(scn.next());
      System.out.println("Which dancer are you?");
      for (int i = 0; i < choreography.getDancers().length; i++) {
        System.out.println((i + 1) + ". " + choreography.getDancers()[i].getName());
      }
      dancerId = choreography.getDancers()[scn.nextInt() - 1].getId();
      System.out.println("Output file:");
      writeExcel(choreography.getScenes(), scn.next(), dancerId);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static Choreography parseChoreographyFromFile(String filename)
      throws FileNotFoundException {
    JsonReader reader = new JsonReader(new FileReader(filename));
    Gson gson = new Gson();
    return gson.fromJson(reader, Choreography.class);
  }

  private static void writeExcel(Scene[] scenes, String excelFilePath, String dancerId)
      throws IOException {
    Workbook workbook = new HSSFWorkbook();
    Sheet sheet = workbook.createSheet();

    int rowCount = 0;

    for (Scene scene : scenes) {
      Row row = sheet.createRow(rowCount++);
      writeScene(scene, row, dancerId);
    }

    try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
      workbook.write(outputStream);
    }
  }

  private static void writeScene(Scene scene, Row row, String dancerId) {
    Cell cell = row.createCell(0);
    cell.setCellValue(scene.getName());
    for (Position position : scene.getPositions()) {
      if (position.getDancer().getRefId().equals(dancerId)) {
        cell = row.createCell(1);
        cell.setCellValue(position.getxPosition());
        cell = row.createCell(2);
        cell.setCellValue(position.getyPosition());
        break;
      }
    }
  }

}
