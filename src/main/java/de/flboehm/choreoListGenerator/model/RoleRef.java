package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for role reference object.
 * An object from this class only has a reference id which references a role object.
 *
 * @author Florian Boehm
 */
public class RoleRef {

  @SerializedName("$ref")
  private String refId;

  /**
   * Getter for refId
   *
   * @return reference ID of role
   */
  public String getRefId() {
    return refId;
  }

  /**
   * Setter for refId
   *
   * @param refId reference ID of role to be set
   */
  public void setRefId(String refId) {
    this.refId = refId;
  }

  @Override
  public String toString() {
    return "RoleRef{" +
        "refId='" + refId + '\'' +
        '}';
  }
}
