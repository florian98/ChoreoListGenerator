package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for position
 *
 * @author Florian Boehm
 */
public class Position {

  @SerializedName("Dancer")
  private DancerRef dancer;

  @SerializedName("X")
  private double xPosition;

  @SerializedName("Y")
  private double yPosition;

  @SerializedName("Movement1X")
  private double movement1x;

  @SerializedName("Movement1Y")
  private double movement1y;

  @SerializedName("Movement2X")
  private double movement2x;

  @SerializedName("Movement2Y")
  private double movement2y;

  /**
   * Empty constructor for position
   */
  public Position() {
  }

  /**
   * Constructor for position
   *
   * @param dancer     Dancer at position
   * @param xPosition  x-coordinate of position
   * @param yPosition  y-coordinate of position
   * @param movement1x x-coordinate of first movement
   * @param movement1y y-coordinate of first movement
   * @param movement2x x-coordinate of second movement
   * @param movement2y y-coordinate of second movement
   */
  public Position(DancerRef dancer, double xPosition, double yPosition, double movement1x,
      double movement1y,
      double movement2x, double movement2y) {
    this.dancer = dancer;
    this.xPosition = xPosition;
    this.yPosition = yPosition;
    this.movement1x = movement1x;
    this.movement1y = movement1y;
    this.movement2x = movement2x;
    this.movement2y = movement2y;
  }

  /**
   * Getter for dancer
   *
   * @return Dancer at position
   */
  public DancerRef getDancer() {
    return dancer;
  }

  /**
   * Setter for dancer
   *
   * @param dancer Dancer at position to be set
   */
  public void setDancer(DancerRef dancer) {
    this.dancer = dancer;
  }

  /**
   * Getter for xPosition
   *
   * @return x-coordinate of position
   */
  public double getxPosition() {
    return xPosition;
  }

  /**
   * Setter for xPosition
   *
   * @param xPosition x-coordinate of position to be set
   */
  public void setxPosition(double xPosition) {
    this.xPosition = xPosition;
  }

  /**
   * Getter for yPosition
   *
   * @return y-coordinate of position
   */
  public double getyPosition() {
    return yPosition;
  }

  /**
   * Setter for yPosition
   *
   * @param yPosition y-coordinate of position to be set
   */
  public void setyPosition(double yPosition) {
    this.yPosition = yPosition;
  }

  /**
   * Getter for movement1x
   *
   * @return x-coordinate of first movement
   */
  public double getMovement1x() {
    return movement1x;
  }

  /**
   * Setter for movement1x
   *
   * @param movement1x x-coordinate of first movement to be set
   */
  public void setMovement1x(double movement1x) {
    this.movement1x = movement1x;
  }

  /**
   * Getter for movement1y
   *
   * @return y-coordinate of first movement
   */
  public double getMovement1y() {
    return movement1y;
  }

  /**
   * Setter for movement1y
   *
   * @param movement1y y-coordinate of first movement to be set
   */
  public void setMovement1y(double movement1y) {
    this.movement1y = movement1y;
  }

  /**
   * Getter for movement2x
   *
   * @return x-coordinate of second movement
   */
  public double getMovement2x() {
    return movement2x;
  }

  /**
   * Setter for movement2x
   *
   * @param movement2x x-coordinate of second movement to be set
   */
  public void setMovement2x(double movement2x) {
    this.movement2x = movement2x;
  }

  /**
   * Getter for movement2y
   *
   * @return y-coordinate of second movement
   */
  public double getMovement2y() {
    return movement2y;
  }

  /**
   * Setter for movement2y
   *
   * @param movement2y y-coordinate of second movement to be set
   */
  public void setMovement2y(double movement2y) {
    this.movement2y = movement2y;
  }

  @Override
  public String toString() {
    return "Position{" +
        "dancer=" + dancer +
        ", xPosition=" + xPosition +
        ", yPosition=" + yPosition +
        ", movement1x=" + movement1x +
        ", movement1y=" + movement1y +
        ", movement2x=" + movement2x +
        ", movement2y=" + movement2y +
        '}';
  }
}
