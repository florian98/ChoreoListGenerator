package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;

/**
 * Class for choreography
 */
public class Choreography {

  @SerializedName("Name")
  private String name;

  @SerializedName("Subtitle")
  private String subtitle;

  @SerializedName("Variation")
  private String variation;

  @SerializedName("Author")
  private String author;

  @SerializedName("Roles")
  private Role[] roles;

  @SerializedName("Dancers")
  private Dancer[] dancers;

  @SerializedName("Scenes")
  private Scene[] scenes;

  /**
   * Empty constructor for choreography
   */
  public Choreography() {
  }

  /**
   * Constructor for choreography
   *
   * @param name      Name of choreography
   * @param subtitle  Subtitle of choreography
   * @param variation Variation of choreography
   * @param author    Author of choreography
   * @param roles     Array of roles in choreography
   * @param dancers   Array of dancers in choreography
   * @param scenes    Array of scenes in choreography
   */
  public Choreography(String name, String subtitle, String variation, String author, Role[] roles,
      Dancer[] dancers, Scene[] scenes) {
    this.name = name;
    this.subtitle = subtitle;
    this.variation = variation;
    this.author = author;
    this.roles = roles;
    this.dancers = dancers;
    this.scenes = scenes;
  }

  /**
   * Getter for name
   *
   * @return Name of choreography
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   *
   * @param name Name of choreography to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for subtitle
   *
   * @return Subtitle of choreography
   */
  public String getSubtitle() {
    return subtitle;
  }

  /**
   * Setter for subtitle
   *
   * @param subtitle Subtitle of choreography to be set
   */
  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  /**
   * Getter for variation
   *
   * @return Variation of choreography
   */
  public String getVariation() {
    return variation;
  }

  /**
   * Setter for variation
   *
   * @param variation Variation of choreography to be set
   */
  public void setVariation(String variation) {
    this.variation = variation;
  }

  /**
   * Getter for author
   *
   * @return Author of choreography
   */
  public String getAuthor() {
    return author;
  }

  /**
   * Setter for author
   *
   * @param author Author of choreography to be set
   */
  public void setAuthor(String author) {
    this.author = author;
  }

  /**
   * Getter for roles
   *
   * @return Array of roles in choreography
   */
  public Role[] getRoles() {
    return roles;
  }

  /**
   * Setter for roles
   *
   * @param roles Array of roles in choreography to be set
   */
  public void setRoles(Role[] roles) {
    this.roles = roles;
  }

  /**
   * Getter for dancers
   *
   * @return Array of dancers in choreography
   */
  public Dancer[] getDancers() {
    return dancers;
  }

  /**
   * Setter for dancers
   *
   * @param dancers Array of dancers in choreography to be set
   */
  public void setDancers(Dancer[] dancers) {
    this.dancers = dancers;
  }

  /**
   * Getter for scenes
   *
   * @return Array of scenes in choreography
   */
  public Scene[] getScenes() {
    return scenes;
  }

  /**
   * Setter for scenes
   *
   * @param scenes Array of scenes in choreography to be set
   */
  public void setScenes(Scene[] scenes) {
    this.scenes = scenes;
  }

  @Override
  public String toString() {
    return "Choreography{" +
        "name='" + name + '\'' +
        ", subtitle='" + subtitle + '\'' +
        ", variation='" + variation + '\'' +
        ", author='" + author + '\'' +
        ", roles=" + Arrays.toString(roles) +
        ", dancers=" + Arrays.toString(dancers) +
        ", scenes=" + Arrays.toString(scenes) +
        '}';
  }
}
