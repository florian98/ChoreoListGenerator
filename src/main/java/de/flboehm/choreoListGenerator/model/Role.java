package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for role
 *
 * @author Florian Boehm
 */
public class Role {

  @SerializedName("$id")
  private String id;

  @SerializedName("ZIndex")
  private int zIndex;

  @SerializedName("Name")
  private String name;

  @SerializedName("Color")
  private String color;

  /**
   * Empty constructor for role
   */
  public Role() {

  }

  /**
   * Constructor for role
   *
   * @param id     Id of role
   * @param zIndex zIndex of role
   * @param name   Name of role
   * @param color  Color of role
   */
  public Role(String id, int zIndex, String name, String color) {
    this.id = id;
    this.zIndex = zIndex;
    this.name = name;
    this.color = color;
  }

  /**
   * Getter for id
   *
   * @return Id of role
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for id
   *
   * @param id Id of role to be set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Getter for zIndex
   *
   * @return zIndex of role
   */
  public int getzIndex() {
    return zIndex;
  }

  /**
   * Setter for zIndex
   *
   * @param zIndex zIndex of role to be set
   */
  public void setzIndex(int zIndex) {
    this.zIndex = zIndex;
  }

  /**
   * Getter for name
   *
   * @return Name of role
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   *
   * @param name Name of role to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for color
   *
   * @return Color of role
   */
  public String getColor() {
    return color;
  }

  /**
   * Setter for color
   *
   * @param color Color of role to be set
   */
  public void setColor(String color) {
    this.color = color;
  }

  @Override
  public String toString() {
    return "Role{" +
        "id='" + id + '\'' +
        ", zIndex=" + zIndex +
        ", name='" + name + '\'' +
        ", color='" + color + '\'' +
        '}';
  }
}
