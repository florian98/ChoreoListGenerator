package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for dancer in choreography
 *
 * @author Florian Boehm
 */
public class Dancer {

  @SerializedName("$id")
  private String id;

  @SerializedName("Role")
  private RoleRef role;

  @SerializedName("Name")
  private String name;

  @SerializedName("Shortcut")
  private String shortcut;

  /**
   * Empty constructor for dancer
   */
  public Dancer() {
  }

  /**
   * Constructor for dancer
   *
   * @param id       Id of dancer
   * @param role     Role of dancer
   * @param name     Name of dancer
   * @param shortcut Shortcut of dancer
   */
  public Dancer(String id, RoleRef role, String name, String shortcut) {
    this.id = id;
    this.role = role;
    this.name = name;
    this.shortcut = shortcut;
  }

  /**
   * Getter for id
   *
   * @return Id of dancer
   */
  public String getId() {
    return id;
  }

  /**
   * Setter for id
   *
   * @param id Id of dancer to be set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Getter for role
   *
   * @return Role of dancer
   */
  public RoleRef getRole() {
    return role;
  }

  /**
   * Setter for role
   *
   * @param role Role of dancer to be set
   */
  public void setRole(RoleRef role) {
    this.role = role;
  }

  /**
   * Getter for name
   *
   * @return Name of dancer
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   *
   * @param name Name of dancer to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for shortcut
   *
   * @return Shortcut of dancer
   */
  public String getShortcut() {
    return shortcut;
  }

  /**
   * Setter for shortcut
   *
   * @param shortcut Shortcut of dancer to be set
   */
  public void setShortcut(String shortcut) {
    this.shortcut = shortcut;
  }

  @Override
  public String toString() {
    return "Dancer{" +
        "id='" + id + '\'' +
        ", role=" + role +
        ", name='" + name + '\'' +
        ", shortcut='" + shortcut + '\'' +
        '}';
  }
}
