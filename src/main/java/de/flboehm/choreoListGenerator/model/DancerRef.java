package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for dancer reference object. An object from this class only has a reference id which
 * references a dancer object.
 *
 * @author Florian Boehm
 */
public class DancerRef {

  @SerializedName("$ref")
  private String refId;

  /**
   * Getter for refId
   *
   * @return reference ID of dancer
   */
  public String getRefId() {
    return refId;
  }

  /**
   * Setter for refId
   *
   * @param refId reference ID of dancer to be set
   */
  public void setRefId(String refId) {
    this.refId = refId;
  }

  @Override
  public String toString() {
    return "DancerRef{" +
        "refId='" + refId + '\'' +
        '}';
  }
}
