package de.flboehm.choreoListGenerator.model;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;

/**
 * Class for scene
 */
public class Scene {

  @SerializedName("Positions")
  private Position[] positions;

  @SerializedName("Name")
  private String name;

  @SerializedName("Timestamp")
  private String timestamp;

  @SerializedName("Text")
  private String text;

  @SerializedName("FixedPositions")
  private boolean fixedPositions;

  /**
   * Empty constructor for scene
   */
  public Scene() {
  }

  /**
   * Constructor for scene
   *
   * @param positions      Array of positions in scene
   * @param name           Name of scene
   * @param timestamp      Timestamp of scene
   * @param text           Text of scene
   * @param fixedPositions Are positions fixed?
   */
  public Scene(Position[] positions, String name, String timestamp, String text,
      boolean fixedPositions) {
    this.positions = positions;
    this.name = name;
    this.timestamp = timestamp;
    this.text = text;
    this.fixedPositions = fixedPositions;
  }

  /**
   * Getter for positions
   *
   * @return Array of all positions in scene
   */
  public Position[] getPositions() {
    return positions;
  }

  /**
   * Setter for positions
   *
   * @param positions Array of all positions in scene to be set
   */
  public void setPositions(Position[] positions) {
    this.positions = positions;
  }

  /**
   * Getter for name
   *
   * @return Name of scene
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   *
   * @param name Name of scene to be set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for timestamp
   *
   * @return Timestamp of scene
   */
  public String getTimestamp() {
    return timestamp;
  }

  /**
   * Setter for timestamp
   *
   * @param timestamp Timestamp of scene to be set
   */
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Getter for text
   *
   * @return Text of scene
   */
  public String getText() {
    return text;
  }

  /**
   * Setter for text
   *
   * @param text Text of scene to be set
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * Getter for isFixedPositions
   *
   * @return Are positions fixed?
   */
  public boolean isFixedPositions() {
    return fixedPositions;
  }

  /**
   * Setter for isFixedPositions
   *
   * @param fixedPositions Are positions fixed? to be set
   */
  public void setFixedPositions(boolean fixedPositions) {
    this.fixedPositions = fixedPositions;
  }

  @Override
  public String toString() {
    return "Scene{" +
        "positions=" + Arrays.toString(positions) +
        ", name='" + name + '\'' +
        ", timestamp='" + timestamp + '\'' +
        ", text='" + text + '\'' +
        ", fixedPositions=" + fixedPositions +
        '}';
  }
}
