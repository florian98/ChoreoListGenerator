# ChoreoListGenerator
A simple tool to generate personal lists from a ChoreoMaster choreography.

:warning: Currently on development! This program is only working rudimentary at the moment.

## What is ChoreoListGenerator
With ChoreoListGenerator you can generate a personal list of all your steps in a choreography from a CHROEO file created with ChoreoMaster.

## Input formats:
- CHOREO file (JSON)

## Output formats:
- xls/xlsx file (Excel)
